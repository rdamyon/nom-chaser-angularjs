import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { DashboardComponent } from "./nomchaser/dashboard/dashboard.component";
import { CityComponent } from "./nomchaser/city/city.component";


@NgModule({
  imports:      [ BrowserModule ],
  declarations: [
    AppComponent,
    DashboardComponent,
    CityComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
