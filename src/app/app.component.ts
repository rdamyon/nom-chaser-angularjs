import { Component } from '@angular/core';

@Component({
  selector: 'nomchaser-app',
  template: `
    <nomchaser-dashboard></nomchaser-dashboard>
    <nomchaser-city></nomchaser-city>
  `,
})

export class AppComponent  {

}
