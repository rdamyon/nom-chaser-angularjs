/**
 * Created by rene on 14.04.17.
 */
import {Component} from "@angular/core";

@Component ({
  selector : 'nomchaser-dashboard',
  templateUrl : './dashboard.component.html'
})

export class DashboardComponent {
  title = 'Nom Chaser';
}
