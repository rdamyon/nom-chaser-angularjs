/**
 * Created by rene on 16.04.17.
 */
export class City {
  displayName : string;
  tag : string;
}

export const CITIES: City[] = [
  { displayName : 'Adelaide', tag : 'adel'},
  { displayName : 'Brisbane', tag : 'bris'},
  { displayName : 'Melbourne', tag : 'mel'},
  { displayName : 'Perth', tag : 'per'},
  { displayName : 'Sydney', tag : 'syd'}
  ];
