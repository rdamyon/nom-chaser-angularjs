/**
 * Created by rene on 14.04.17.
 */
import {Component} from "@angular/core";
import {City, CITIES} from "./city"

@Component ({
  selector : 'nomchaser-city',
  templateUrl : './city.component.html',
  styleUrls : ['./city.component.css']
})

export class CityComponent {
  cities : City[] = CITIES;

}
